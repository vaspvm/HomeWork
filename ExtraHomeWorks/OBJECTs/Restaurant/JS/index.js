/* Написать функцию-помощник для ресторана.
 * Функция обладает двумя параметрами:
 * - Размер заказа (small, medium, large);
 * - Тип обеда (breakfast, lunch, dinner).
 * Функция возвращает объект с двумя полями:
 * - totalPrice — общая сумма блюда с учётом его размера и типа;
 * - totalCalories — количество калорий, содержащееся в блюде с учётом его размера и типа.
 * - Дополнительные проверки совершать не нужно; */

const priceList = {
  sizes: {
    small: {
      price: 15,
      calories: 250
    },
    medium: {
      price: 25,
      calories: 340
    },
    large: {
      price: 35,
      calories: 440
    }
  },
  types: {
    breakfast: {
      price: 4,
      calories: 25
    },
    lunch: {
      price: 5,
      calories: 5
    },
    dinner: {
      price: 10,
      calories: 50
    }
  }
};

let size = prompt("Enter size of meal (small / medium / large)");
let type = prompt("Enter type of meal (breakfast / lunch / dinner)");

function restHelper(size, type) {
  let sumPrice = 0;
  let sumCalor = 0;

  function funcSize(size) {
    sumPrice = priceList.sizes[size].price;
    sumCalor = priceList.sizes[size].calories;
  }
  function funcType(type) {
    sumPrice += priceList.types[type].price;
    sumCalor += priceList.types[type].calories;
  }
  switch (size) {
    case "small":
      funcSize(size);
      break;
    case "medium":
      funcSize(size);
      break;
    case "large":
      funcSize(size);
      break;
  }

  switch (type) {
    case "breakfast":
      funcType(type);
      break;
    case "lunch":
      funcType(type);
      break;
    case "dinner":
      funcType(type);
      break;
  }

  return {
    totalPrice: sumPrice,
    totalCalories: sumCalor
  };
}

let total = restHelper(size, type);
console.log(total);
