/* Задание 1.
 *
 * Написать функцию-сумматор всех своих параметров.
 *
 * Функция принимает произвольное количество параметров.
 * Однако каждый из них обязательно должен быть числом.
 *
 * Генерировать ошибку, если:
 * - Если функция была вызвана менее, чем с двумя аргументами;
 * - Хоть один из аргументов не является допустимым числом (в ошибке указать его порядковый номер).
 *
 * Условия:
 * - Использовать тип функции arrow function;
 * - Использовать объект arguments запрещено.*/

//  const summAll = (...arg) => {
//      arg.reduce(0, 1, 0, {
//          if (+currentValue.isNaN) {
//             return arg.currentIndex;
//          }
//      });
//  }

 let recipeMap = new Map([
    ["огурец", 500],
    ["помидор", 350],
    ["лук",    50]
  ]);
  
  // перебор по ключам (овощи)
  for (let vegetable of recipeMap.keys()) {
    // console.log(vegetable); // огурец, помидор, лук
  }
  
  // перебор по значениям (числа)
  for (let amount of recipeMap.values()) {
    // console.log(amount); // 500, 350, 50
  }
  
  // перебор по элементам в формате [ключ, значение]
  for (let entry of recipeMap) { // то же самое, что и recipeMap.entries()
    // console.log(entry); // огурец,500 (и так далее)
  }

  const testObj = {
      name: "olen",
      name2: "olen2",
      age: 25,
      age2: 27,
  };

  // console.log(Object.keys(testObj));
  // console.log(Object.values(testObj));
  // console.log(Reflect.ownKeys(testObj));


    let list = document.querySelectorAll('.store li');
// console.log(list);
    for(item of list){
      let [description, amount] = item.innerText.split(' ');
      if(amount === '0'){
        amount = 'Закончился'

        item.innerText = `${description} ${amount}`;

        item.style.color = 'red';
        item.style.fontWeight = 600;
      }
    }
//  console.log(list);
