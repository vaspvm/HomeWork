/* ЗАДАНИЕ - 1 (Data checking)
 * Пользователь должен ввести Имя и свой возраст.
 * В случае если он вводит не соответствующие данные, нужно переспрашивать его,
 * ДО ТЕХ ПОР ПОКА данные не будут введены корректно.
 * Не корректными данными считается: число при вводе имени и буквенные символы при вводе возраста*/

let name, age;

do {
  name = prompt("Enter your name");
} while (Number(+name) || name === "" || typeof name !== "string");

do {
  age = +prompt("Enter your age");
} while (age <= 1 || isNaN(age));

document.write(`Name - ${name}, Age - ${age}`);
