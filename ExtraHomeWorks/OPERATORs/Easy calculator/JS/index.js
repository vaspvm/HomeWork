/* ЗАДАНИЕ - 3 (Easy calculator)
 * Осуществляем проверку на корректность введения данных.
 * Пользователь должен ввести два числа и символ операции.
 * Если пользователь ввел НЕ числа или операцию, которой нет в списке - спрашиваем все по новой,
 * ДО ТЕХ ПОР ПОКА не введет правильно.
 *  Список операций:
 *   * - умножение
 *   + - добавление
 *   - - отнимание
 *   / - деление */

let numb1, numb2, opr;

do {
  numb1 = +prompt("Enter first number");
} while (isNaN(numb1) || numb1 == 0);

do {
  numb2 = +prompt("Enter second number");
} while (isNaN(numb2) || numb2 == 0);

do {
  opr = prompt("Enter operation: + - * /");
} while (opr != "+" && opr != "-" && opr != "*" && opr != "/");

console.log(`${numb1} ${opr} ${numb2}`);
