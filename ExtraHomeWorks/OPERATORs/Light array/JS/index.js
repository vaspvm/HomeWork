/*  ЗАДАНИЕ-5 (Light array)
 *  Дан массив [‘Gogi’, ‘Goga’, ‘Gogo’, ‘Gugu’, ‘Gunigugu’, ‘Guguber’, ‘Gigi’].
 *  Вывести в консоль по очереди все значение которые в нем есть ДВУМЯ способами.*/

const arr1 = ["Gogi", "Goga", "Gogo", "Gugu", "Gunigugu", "Guguber", "Gigi"];

for (let i = 0; i < arr1.length; i++) {
  console.log(arr1[i]);
}

let y = 0;
while (y < arr1.length) {
  console.log(arr1[y]);
  y++;
}
