/*Пользователь вводит число и программа должна отобразить в документе пирамиду (равнобедренный треугольник) из * заданной высоты.*/

// let h = +prompt("Enter the height of the pyramid");
// document.write(`Height = ${h} <br/><br/>`);

// let a, a1, b;
// b = h - (h - 1);
// for (; h > 0; h--) {
//   for (a = 0; a < h - 1; a++) {
//     document.write(" &nbsp; ");
//   }
//   for (a1 = 0; a1 < b; a1++) {
//     document.write("*");
//   }
//   b = b + 2;
//   document.write("<br/>");
// }

/*Пользователь вводит число и программа должна отобразить в консоле пирамиду (равнобедренный треугольник) из * заданной высоты.*/

let h = +prompt("Enter the height of the pyramid");
console.log(`Height = ${h} \n`);

let a, a1, b;
b = h - (h - 1);
for (; h > 0; h--) {
  let r = "",
    r1 = "";
  for (a = 0; a < h - 1; a++) {
    r += " ";
  }
  for (a1 = 0; a1 < b; a1++) {
    r1 += "*";
  }
  console.log(r + r1);
  b = b + 2;
}
