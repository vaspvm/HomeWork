/* ЗАДАНИЕ - 4 (Сalculator)
 * Спрашиваем у пользователя два числа и операцию, которую над ними нужно совершить.
 * Осуществляем проверку на корректность введения данных из предыдущего задания.
 * Как результат выводим на экран сообщение с результатом выбранной операции над введенными числами.
 *
 * ПРОДВИНУТАЯ СЛОЖНОСТЬ:
 *     - к списку операций добавить -> возведение в степень, взятие корня числа №1 степени числа №2.
 *     - хранить в памяти результат последней операции. Если вместо одного из двух чисел
 * введено `PREV_OP`
 *       подставлять вместо него результат предыдущей операции
 *     - “обернуть” код калькулятора в функцию, которая принимает три аргумента - число1, число2, операция.*/

let numb1, numb2, opr;

let value, oldValue;

while (numb1 == 0 || isNaN(numb1)) {
  let numb = prompt("Enter your first number");
  if (numb === "PREV_OP") {
    numb1 = oldValue;
    break;
  }
  numb1 = +numb;
}
console.log(numb1);

while (numb2 == 0 || isNaN(numb2)) {
  let numb = prompt("Enter your second number");
  if (numb === "PREV_OP") {
    numb2 = oldValue;
    break;
  }
  numb2 = +numb;
}
console.log(numb2);

do {
  opr = prompt("Enter your operation: + - * / ** ");
} while (opr != "+" && opr != "-" && opr != "*" && opr != "/" && opr != "**");
console.log(opr);

switch (opr) {
  case "+":
    value = numb1 + numb2;
    break;
  case "-":
    value = numb1 - numb2;
    break;
  case "*":
    value = numb1 * numb2;
    break;
  case "/":
    value = numb1 / numb2;
    break;
  case "**":
    value = numb1 ** numb2;
    break;
}

document.write(`${numb1} ${opr} ${numb2} = ${value}`);
oldValue = value;

// с сохранением прошлого ответа пока не получилось, по возможности еще попробую в будущем
