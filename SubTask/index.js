//проверка вводимого числа: число, больше 0, целое
function checkNumber(number) {
  if (isNaN(number) || number <= 0 || parseFloat(number) !== parseInt(number)) {
    return true;
  } else {
    return false;
  }
}

// проверка: нажатие ОК или Отмена без ввода информации
function checkFilling(answer) {
  if (answer == "" || answer == null) {
    return true;
  } else {
    return false;
  }
}

let num;
do {
  num = +prompt("Enter number", num);
} while (checkNumber(num));
