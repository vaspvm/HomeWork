/*Задание
Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

В файле index.html лежит разметка для двух полей ввода пароля.
По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

После нажатия на кнопку страница не должна перезагружаться
Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.*/

const firstPass = document.querySelector(".firstPassword");
const confirmPass = document.querySelector(".confirmPassword");

const iconInFirstPass = document.querySelector(".firstPassword")
  .nextElementSibling;
const iconInConfirmPass = document.querySelector(".confirmPassword")
  .nextElementSibling;

const buttonSubmit = document.querySelector(".btn");

const formWithPassword = document.querySelector("form");

const errMassage = document.createElement("span");
errMassage.style.color = "red";
errMassage.innerHTML = `Нужно ввести одинаковые значения`;

const visiblePassword = function (event) {
  if (event.target.classList.contains("fa-eye")) {
    event.target.previousElementSibling.setAttribute("type", "text");
  } else {
    event.target.previousElementSibling.setAttribute("type", "password");
  }
};

const checkPassword = function (event) {
  console.log(firstPass.value);
  console.log(confirmPass.value);
  if (firstPass.value == confirmPass.value) {
    alert("You are welcome");
  } else {
    buttonSubmit.before(errMassage);
  }
};

const changeIcon = function (event) {
  event.target.classList.toggle("fa-eye");
  event.target.classList.toggle("fa-eye-slash");
};

const enterPassword = function (event) {
  event.preventDefault();
  errMassage.remove(buttonSubmit);
  if (event.target == iconInFirstPass || event.target == iconInConfirmPass) {
    changeIcon(event);
    visiblePassword(event);
  }
  if (event.target == buttonSubmit) {
    checkPassword(event);
  }
};

formWithPassword.addEventListener("click", enterPassword);
