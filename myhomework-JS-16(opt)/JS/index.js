/* Задание
 * Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.
 * Задача должна быть реализована на языке javascript, без использования
 * фреймворков и сторонник библиотек (типа Jquery).*/

/* Технические требования:
 * Написать функцию для подсчета n-го обобщенного числа Фибоначчи.
 * Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два
 * числа последовательности (могут быть любыми целыми числами), n - порядковый
 * номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться
 * по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
 * Считать с помощью модального окна браузера число, которое введет пользователь (n).
 * С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи
 * и вывести его на экран.
 * Пользователь может ввести отрицательное число - результат надо посчитать по
 * такому же правилу (F-1 = F-3 + F-2).*/

// проверка: число, целое
function checkNumber(number) {
  if (isNaN(number) || parseFloat(number) !== parseInt(number)) {
    return true;
  } else {
    return false;
  }
}

let num1, num2, numN;
do {
  num1 = +prompt("Enter first number", num1);
} while (checkNumber(num1));

do {
  num2 = +prompt("Enter second number", num2);
} while (checkNumber(num2));

do {
  numN = +prompt("Enter index", numN);
} while (checkNumber(numN) || numN == 0); //добавил проверку, что бы индекс не был =0

function searchFibaPlus(n1, n2, nX) {
  let value;
  for (let i = 2; i < nX; i++) {
    value = n1 + n2;
    n1 = n2;
    n2 = value;
  }
  return console.log(
    `First number = ${num1}, Second number = ${num2}, index = ${numN}, Fibonacci number = ${n2}`
  );
}

searchFibaPlus(num1, num2, numN);
