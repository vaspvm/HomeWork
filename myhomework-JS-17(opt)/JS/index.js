/*Задание
Создать объект студент "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Создать пустой объект student, с полями name и last name.
Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.*/

const checkFilling = function (answer) {
  if (answer == "" || answer == null) {
    return true;
  } else {
    return false;
  }
};
const getData = function (question) {
  let data;
  do {
    data = prompt(question);
  } while (checkFilling(data));
  return data;
};

const student = {
  name: getData("Enter your name"),
  lastName: getData("Enter your second name"),
  table: {},
};

let value = 0; //счетчик для суммы баллов
let counter = 0; //счетчик кол-ва предметов
let counter2 = 0; //счетчик кол-ва баллов, > 4

// опрос предметов и оценок
do {
  subject = prompt("Enter name subject");
  if (subject === null) break;
  student.table[subject] = +prompt(`Enter value by subject`);
  counter++; //кол-во предметов
} while (true);

//просчет суммы баллов и кол-ва баллов > 4
for (let prop in student.table) {
  value += student.table[prop];
  if (student.table[prop] < 4) {
    counter2++;
  }
}
console.log(student);
console.log(`${counter2} grades < 4`);
if (counter2 < 1) console.log("Student transferred to next course");
console.log(`Grade point average = ${value / counter}`);
if (value / counter > 7) console.log("Student granted scholarship");
