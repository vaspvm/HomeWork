/*Теоретический вопрос
 * Опишите своими словами, что такое метод обьекта*/

/*Задание
 * Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без * использования фреймворков и сторонник библиотек (типа Jquery).*/

/*Технические требования:
 * Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
 * При вызове функция должна спросить у вызывающего имя и фамилию.
 * Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
 * Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
 * Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.*/

/*Необязательное задание продвинутой сложности:
 * Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName () и setLastName(), которые позволят изменить данные свойства.*/

// проверка: нажатие ОК или Отмена без ввода информации
function checkFilling(answer) {
  if (answer == "" || answer == null) {
    return true;
  } else {
    return false;
  }
}

// auto-prompt with check
const getData = function (question) {
  let data;
  do {
    data = prompt(question);
  } while (checkFilling(data));
  return data;
};

const createNewUser = function () {
  let newUser = {
    firstName: getData("Enter your name"),
    lastName: getData("Enter your second name"),
  };
  Object.defineProperty(newUser, "getLogin", {
    get() {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },
  });
  return newUser;
};
let user = createNewUser();
console.log(user.getLogin);
