/*Теоретический вопрос
Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
Позволяет прочитать символ как обычную строку.

Задание
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

Технические требования:
Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.*/

function checkFilling(answer) {
  if (answer == "" || answer == null) {
    return true;
  } else {
    return false;
  }
}

const getData = function (question) {
  let data;
  do {
    data = prompt(question);
  } while (checkFilling(data));
  return data;
};

const createNewUser = function () {
  const newUser = {
    firstName: getData("Enter your name"),
    lastName: getData("Enter your second name"),
    birthday: new Date(prompt("Enter your birthday (YYYY-MM-DD)")),
  };
  Object.defineProperty(newUser, "getLogin", {
    get() {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },
  });

  newUser.getAge = function () {
    return new Date().getFullYear() - this.birthday.getFullYear();
  };

  Object.defineProperty(newUser, "getPassword", {
    get() {
      return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`;
    },
  });

  return newUser;
};
const user = createNewUser();
console.log(user);
console.log(user.getLogin);
console.log(user.getAge());
console.log(user.getPassword);
