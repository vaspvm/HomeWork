/*Теоретический вопрос
Опишите своими словами как работает цикл forEach.
Перебирает все элементы массива и применяет к каждому переданную функцию.

Задание
Реализовать функцию фильтра массива по указанному типу данных. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].*/

const generalArray = ["hello", 25, null, "world", 55, true, "John", false];

console.log(generalArray);

const filterBy = function (data, sort) {
  let a = 0; //counter for newArray
  let newArray = [];
  for (let i in data) {
    // debugger
    // console.log(typeof data[i]);

    if (typeof data[i] != sort) {
      newArray[a] = data[i];
      a++;
    }
  }
  return newArray;
};
//typeof(null) === object
clearArray = filterBy(generalArray, "string");
console.log(clearArray);


