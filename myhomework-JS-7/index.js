/*Задание
Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
Создать функцию, которая будет принимать на вход массив.
Каждый из элементов массива вывести на страницу в виде пункта списка
Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.

Примеры массивов, которые можно выводить на экран:
['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
['1', '2', '3', 'sea', 'user', 23]
Можно взять любой другой массив.

Необязательное задание продвинутой сложности:
??? Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.*/

const arrSimple = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arrComplex = [
  "hello",
  "world",
  ["1", "2", "3", "sea", "user", 23],
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
  {
    name: "Ivan",
    age: 30,
  },
];

const showList = (arr) => {
  const ul = document.createElement("ul");
  document.body.prepend(ul);
  const listToAdd = arr.map((elem) => {
    if (Array.isArray(elem)) {
      let subListToAdd = "";
      for (let item of elem) {
        subListToAdd += `<li>${item}</li>`;
      }
      return `<li><ul>${subListToAdd}</ul></li>`;
    }
    if (typeof elem === "object") {
      let subListToAdd = "";
      for (let item of Object.entries(elem)) {
        subListToAdd += `<li>${item}</li>`;
      }
      return `<li><ul>${subListToAdd}</ul></li>`;
    }
    return `<li>${elem}</li>`;
  });
  ul.innerHTML += listToAdd.join("");
};

showList(arrComplex);
