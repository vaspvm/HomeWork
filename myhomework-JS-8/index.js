/*
Задание
Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
Поведение поля должно быть следующим:

+ При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. 
+ При потере фокуса она пропадает.
+ Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. 
+ Рядом с ним должна быть кнопка с крестиком (X). 
+ Значение внутри поля ввода окрашивается в зеленый цвет.
+ При нажатии на Х - span с текстом и кнопка X должны быть удалены. 
+ Значение, введенное в поле ввода, обнуляется.
+ Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. 
+ Span со значением при этом не создается.

В папке img лежат примеры реализации поля ввода и создающегося span.
*/

const container = document.createElement("div");
document.body.append(container);
container.style.cssText = `
width: 750px;
margin: auto;
margin-top: 50px;
`;

const boxForSpan = document.createElement("div");
container.append(boxForSpan);
boxForSpan.style.cssText = `
padding-left: 210px;
display: hidden;
`;

const spanWithPrice = document.createElement("span");
boxForSpan.append(spanWithPrice);
spanWithPrice.style.cssText = `
font-size: 36px;
`;

const button = document.createElement("button");
button.innerHTML = "X";
button.style.marginLeft = "15px";

const contForInput = document.createElement("div");
container.append(contForInput);
contForInput.style.cssText = `
margin-top: 50px;
padding-left: 10px;
`;

const boxForTextForInput = document.createElement("div");
contForInput.append(boxForTextForInput);
boxForTextForInput.innerHTML = `Enter price:`;
boxForTextForInput.style.cssText = `
width: 200px;
font-size: 42px;
display: inline-block;
`;

const input = document.createElement("input");
contForInput.append(input);
input.value = "Price";
input.style.cssText = `
font-size: 42px;
border: 1px solid black;
`;

const spanWithError = document.createElement("span");
spanWithError.innerHTML = "Please enter correct price.";
container.append(spanWithError);
spanWithError.style.fontSize = "36px";
spanWithError.style.display = "none";

const changeBorderColor = function () {
  input.style.outline = "none";
  input.style.border = "5px solid green";
};

const showSpanWithPrice = function () {
  if (+input.value > 0) {
    spanWithPrice.innerHTML = `Current price = ${input.value}`;
    input.style.color = "green";
    boxForSpan.append(button);
    boxForSpan.style.display = "block";
  } else {
    input.style.border = "5px solid red";
    spanWithError.style.display = "inline";
  }
};

const deleteSpanWithPrice = function () {
  boxForSpan.style.display = "none";
  input.value = "";
  input.style.color = "black";
  input.style.border = "1px solid black";
};

const loseFocus = function () {
  input.style.border = "1px solid black";
  spanWithError.style.display = "none";

  showSpanWithPrice();
};

input.addEventListener("focus", changeBorderColor);

input.addEventListener("blur", loseFocus);

button.addEventListener("click", deleteSpanWithPrice);
