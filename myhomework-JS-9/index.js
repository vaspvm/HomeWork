/*
Задание
Реализовать переключение вкладок (табы) на чистом Javascript.

Технические требования:

В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.
*/


const tabsFather = document.querySelector(".tabs");
const tabsContent = document.querySelector(".tabs-content");
const buttoms = tabsFather.children;
const textContent = tabsContent.children;

const activeTab = function (event) {
  for (buttom of event.currentTarget.children) {
    buttom.classList.remove("active");
  }
  event.target.classList.add("active");

  for (text of textContent) {
    text.classList.add("noVisibility");
  }
  let activText = Array.from(buttoms).findIndex((elem) =>
    elem.classList.contains("active")
  );
  textContent[activText].classList.remove("noVisibility");
};

tabsFather.addEventListener("click", activeTab);
