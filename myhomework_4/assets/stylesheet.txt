.sprite {
    background-image: url(spritesheet.png);
    background-repeat: no-repeat;
    display: block;
}

.sprite-Google_ {
    width: 25px;
    height: 16px;
    background-position: -5px -5px;
}

.sprite-facebook {
    width: 10px;
    height: 20px;
    background-position: -40px -5px;
}

.sprite-icon1 {
    width: 19px;
    height: 19px;
    background-position: -60px -5px;
}

.sprite-icon1-hover {
    width: 19px;
    height: 19px;
    background-position: -5px -34px;
}

.sprite-icon2 {
    width: 28px;
    height: 20px;
    background-position: -34px -35px;
}

.sprite-icon2-hover {
    width: 28px;
    height: 20px;
    background-position: -89px -5px;
}

.sprite-icon3 {
    width: 20px;
    height: 20px;
    background-position: -72px -35px;
}

.sprite-icon3-hover {
    width: 20px;
    height: 20px;
    background-position: -5px -65px;
}
